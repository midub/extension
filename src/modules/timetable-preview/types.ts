export enum TimetableEntryType {
    Lecture,
    Seminar,
    Unknown
}

export interface RegistrationTimetableEntry {
    type: TimetableEntryType,
    day: number,
    start: number,
    end: number,
    time: string,
    link: string | null
}

export interface RegisteredTimetableEntry extends RegistrationTimetableEntry {
    name: string,
    code: string,
}

export interface AvailableTimetableEntry extends RegistrationTimetableEntry {
    highlighted: boolean,
    collision: boolean,
    index: number
}

export interface TimetableEntryLayout {
    offset: number,
    width: number
}
