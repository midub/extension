import moment from "moment";
import { Box } from "@chakra-ui/react";
import React from "react";
import { FC } from "react"

export interface SubmissionReminderProps {
    reminder: Date,
    onCancel: () => void
}

export const SubmissionReminder: FC<SubmissionReminderProps> = ({ reminder, onCancel }: SubmissionReminderProps) => {
    return (
        <Box alignSelf="stretch" borderWidth="1px" borderRadius="lg" display="flex" flexFlow="row nowrap" justifyContent="space-between" alignItems="center">
            {moment(reminder).locale("cs").format("LLL")}
            <button onClick={() => onCancel()}>Zrušit</button>
        </Box>
    );
};

